import React from 'react';
import './App.css';
import ZoomMain from './components/ZoomMain';
import {
	BrowserRouter as Router,
	Switch,
	Route,
} from 'react-router-dom';
import MeetingZoom from './components/MeetingZoom';
import ZoomCreateRoom from './components/ZoomCreateRoom';

function App() {
	return (
		<div className="App">
			<Router>
				<Switch>
					<Route exact path="/join" >
						<ZoomMain />
					</Route>
					<Route path="/create">
						<ZoomCreateRoom />
					</Route>
					<Route path="/meeting">
						<MeetingZoom />
					</Route>
				</Switch>
			</Router>
		</div>
	);
}

export default App;
