import React from 'react';
import "./css/MeetingZoom.css";

export default class MeetingZoom extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			client_id: "hXYl1_4oSgeGthd4teyPyw",
			domain_url: "https://e68ad6b7.ngrok.io/create",
			href: ""
		}
	}
	componentDidMount(){
		let href = "https://zoom.us/oauth/authorize?response_type=code&client_id="+this.state.client_id+"&redirect_uri="+this.state.domain_url;
		this.setState({href: href});
	}
	render(){
		return(
			<div className="meeting-center">
				<h1>Create a new Meeting</h1>
				<a href={this.state.href}
					target="_blank" 
					rel="noopener noreferrer">
					<img src="https://marketplacecontent.zoom.us/zoom_marketplace/img/add_to_zoom.png" height="32" alt="Add to ZOOM" />
				</a>
			</div>
		)
	}
}