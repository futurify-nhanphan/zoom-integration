import React from 'react';
import { ZoomMtg } from '@zoomus/websdk';

// Add this, never use it client side in production
const API_KEY = 'gS7w7-c0SlKtTXRGmd5Z7g';
// Add this, never use it client side in production
const API_SECRET = 'X7br2MnnRMZz1vfAB0DEQYzY35zfhFpGPSPa';
// This can be your Personal Meeting ID
const MEETING_NUMBER = 96844152220;

const meetConfig = {
    apiKey: API_KEY,
    apiSecret: API_SECRET,
    meetingNumber: MEETING_NUMBER,
    userName: 'test user',
    passWord: '502402',
    leaveUrl: 'https://zoom.us',
    role: 0
};
export default class ZoomMain extends React.Component {
	state = {
		meetingLaunched: false,
		signature: ""
    }

    launchMeeting = () => {
        
        // change state of meeting
        this.setState({ meetingLaunched: !this.state.meetingLaunched })

		// generateSignature should only be used in development
		if(this.state.signature !== null){
			ZoomMtg.init({
				leaveUrl: "http://www.zoom.us",
				isSupportChat: true,
				isSupportAV: true,
				screenShare: true,
				success: () => {
				ZoomMtg.join({
					meetingNumber: MEETING_NUMBER,
					userName: meetConfig.userName,
					signature: this.state.signature,
					apiKey: meetConfig.apiKey,
					userEmail: "",
					passWord: meetConfig.passWord,
					success: function(res) {
						// eslint-disable-next-line
						console.log("join meeting success");
					},
					error: function(res) {
						// eslint-disable-next-line
						console.log(res);
					}
				});
				},
				error: function(res) {
				// eslint-disable-next-line
				console.log(res);
				}
			});
		}
    }
    
    componentDidMount() {
        ZoomMtg.setZoomJSLib("https://source.zoom.us/1.7.5/lib", "/av");
        ZoomMtg.preLoadWasm();
		ZoomMtg.prepareJssdk();
		let signature = ZoomMtg.generateSignature({
			meetingNumber: MEETING_NUMBER,
			apiKey: API_KEY,
			apiSecret: API_SECRET,
			role: meetConfig.role,
			success: function(res) {
			  // eslint-disable-next-line
			  console.log("success signature: " + res.result);
			}
		  });
		this.setState({signature});
    }

    render() {
        const { meetingLaunched} = this.state;
        // Displays a button to launch the meeting when the meetingLaunched state is false
        return (
            <>
                {!meetingLaunched ? 
                    <button className="launchButton" onClick={this.launchMeeting}>Launch Meeting</button> 
                : 
                    <></>
                }
            </>
        )
    }
}